//
//  ShoppingItemMO+CoreDataProperties.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 26.10.21.
//
//

import Foundation
import CoreData


extension ShoppingItemMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShoppingItemMO> {
        return NSFetchRequest<ShoppingItemMO>(entityName: "ShoppingItem")
    }

    @NSManaged public var categoryColor: String?
    @NSManaged public var name: String?

}

extension ShoppingItemMO : Identifiable {

}
