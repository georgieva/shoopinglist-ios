//
//  CoreDataPersistable.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 25.10.21.
//

import Foundation
import CoreData

protocol CoreDataPersistable {
    var entityName: String{get}
    func fillInFields(entity: NSManagedObject)
}

extension CoreDataPersistable {
    var entityName: String {
        return "\(String.init(describing: type(of: self)))"
    }
}
