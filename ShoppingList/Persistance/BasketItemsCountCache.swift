//
//  BasketItemsCountCache.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 25.10.21.
//

import Foundation

class BasketItemsCountCache {
    
    static let countChangedNotification = Notification.Name("BasketCountChangedNotification")
    
    static let instance = BasketItemsCountCache()
    
    private(set) var count: Int = 0 {
        didSet {
            NotificationCenter.default.post(name: BasketItemsCountCache.countChangedNotification, object: nil)
        }
    }
    
    private init () {
        let context = CoreDataController.instance.persistentContainer.viewContext
        do {
         try self.count = context.count(for: ShoppingItemMO.fetchRequest())
        }
        catch {
            print(error)
        }
    }
    
    func increaseCountIfNeeded(entity:CoreDataPersistable){
        if entity is ShoppingItem {
            count += 1
        }
    }
}
