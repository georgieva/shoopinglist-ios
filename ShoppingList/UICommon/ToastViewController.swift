//
//  ToastViewController.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import UIKit

class ToastViewController: UIViewController {
    
    @IBOutlet private weak var toastLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet private weak var toastLabelHideConstant: NSLayoutConstraint!
    @IBOutlet private var toastLabelTopConstraint: NSLayoutConstraint!
    private var hideTimer: Timer?
    
    //MARK:- factory
    static func setupToastViewController()-> ToastViewController {
        return ToastViewController.init(nibName: String(describing: self), bundle: nil)
    }
    
    //MARK:- general Toast Showing
    static func showToast(text: String, color: UIColor, duration: TimeInterval, inWindow window: UIWindow?) {
        let toastControllerForWindow = (window?.windowScene?.delegate as? SceneDelegate)?.toastController
        toastControllerForWindow?.showToast(text: text, color: color, duration: duration)
    }
    
    //MARK:- overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.isHidden = true
    }
    
    deinit {
            hideTimer?.invalidate()
    }
    
    //MARK:- toast animaions
    func showToast(text: String, color: UIColor, duration: TimeInterval){
        
        hideTimer?.invalidate()
        hideTimer = nil
        
        toastLabel.text = text
        backgroundView.backgroundColor = color
        
        self.backgroundView.layer.removeAllAnimations()
        
        if backgroundView.isHidden {
            toastLabelTopConstraint.isActive = false
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            backgroundView.isHidden = false
            self.backgroundView.alpha = 1
            toastLabelTopConstraint.isActive = true
            UIView.animate(withDuration: 0.3) {[weak self] in
                self?.view.layoutIfNeeded()
                
            }
        }
        else {
            self.backgroundView.alpha = 1
            self.view.layoutIfNeeded()
        }
        
        hideTimer = Timer.scheduledTimer(withTimeInterval: duration, repeats: false, block: { [weak self] (timer) in
                    self?.hideToast()
                })
        
    }
    
    @objc
    @IBAction func hideToast() {
        UIView.animate(withDuration: 0.3) {
            self.backgroundView.alpha = 0
        } completion: { (_) in
            self.backgroundView.isHidden = true
            self.backgroundView.alpha = 1
        }
        
        hideTimer?.invalidate()
        hideTimer = nil
    }

}
