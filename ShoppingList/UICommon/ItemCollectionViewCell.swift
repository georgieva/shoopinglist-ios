//
//  ItemCollectionViewCell.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    static let cellID = String(describing: ItemCollectionViewCell.self)
    
    static var nib: UINib {
        return UINib(nibName:"ItemCollectionViewCell", bundle: nil)
    }
    
    @IBOutlet weak var label: UILabel!
    
    var maxWidthConstraint: NSLayoutConstraint? {
        willSet {
            maxWidthConstraint?.isActive = false
        }
    }
    @IBOutlet weak var topAndBottomMargin: NSLayoutConstraint!
    
    static func register(collectionView: UICollectionView) {
        collectionView.register(nib, forCellWithReuseIdentifier: cellID)
    }
    
    override var isSelected: Bool {
        didSet {
            self.label.backgroundColor = isSelected && self.backgroundColor == nil ? UIColor.tertiarySystemFill : UIColor.clear
        }
    }
}
