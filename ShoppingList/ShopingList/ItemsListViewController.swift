//
//  ItemsListViewController.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import UIKit

class ItemsListViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    
    var category: ShoppingCategory? {
        didSet {
            self.title = category?.name
            if collectionView != nil {
                updateSnapshot()
            }
        }
    }

    private lazy var dataSource: UICollectionViewDiffableDataSource<Int, ShoppingItem> = {
        return UICollectionViewDiffableDataSource<Int, ShoppingItem>(collectionView: collectionView) {
            [weak self] (collectionView: UICollectionView, indexPath: IndexPath, shoppingItem: ShoppingItem) -> UICollectionViewCell? in
            
            guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ItemCollectionViewCell.cellID,
                    for: indexPath) as? ItemCollectionViewCell else {
                fatalError("Cannot create new cell")
            }
            
            cell.label.text = shoppingItem.name
            cell.contentView.backgroundColor = self?.category?.colour

            return cell
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.collectionViewLayout = ItemListUtil.twoColumnLayout()
        ItemCollectionViewCell.register(collectionView: collectionView)
        collectionView.dragInteractionEnabled = true
        updateSnapshot()
    }
    
    func updateSnapshot() {
        if category?.items?.isEmpty == true {
            collectionView.isHidden = true
        }
        else {
            collectionView.isHidden = false
        var snapshot = NSDiffableDataSourceSnapshot<Int, ShoppingItem>()
        snapshot.appendSections([0])
        if let items = category?.items {
            snapshot.appendItems(items)
        }
        dataSource.apply(snapshot, animatingDifferences: false)
        }
    }
}

extension ItemsListViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        var shoppingItem = category?.items?[indexPath.item]
        shoppingItem?.colour = category?.colour
        let itemProvider = NSItemProvider(object: (shoppingItem?.name ?? NSLocalizedString("item unavailable", comment: "item that was removed from the server")) as NSItemProviderWriting)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = shoppingItem
        return [dragItem]
    }
}
