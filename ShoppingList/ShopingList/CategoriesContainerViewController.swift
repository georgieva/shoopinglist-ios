//
//  ViewController.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//


//TODO: - simplify and divide into single responsibility chunks. Separate(abstract) infinite bars from the data case at hand
import UIKit

private let cellID = "tabCell"

class CategoriesContainerViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UICollectionViewDelegate, UIDropInteractionDelegate, UIGestureRecognizerDelegate {
    
    private var categories: [ShoppingCategory]? {
        didSet {
            guard let categories = self.categories, !categories.isEmpty else {
                selection = nil
                return
            }
            let selectedIndex = categories.firstIndex {
                //using category name as identifier which is not super correct, but a real world example will probably have ids
                $0.name == selection?.category.name
            }
            if let selectedIndex = selectedIndex{
                self.selection = (selectedIndex, categories[selectedIndex])
            }
            else{
                self.selection = (0, categories[0])
            }
        }
    }
    
    //this is only nil when categories are not loaded. can we safely force-unwrap it
    private var selection: (index:Int, category:ShoppingCategory)?
    
    private weak var pageVC: UIPageViewController!
    @IBOutlet private weak var tabsCollectionView: UICollectionView!
    
    @IBOutlet private weak var shoppingBasketButton: UIButton!
    @IBOutlet private  var shoppingBasketConstraints: [NSLayoutConstraint]!
    private var  basketDragRecognizer: UIPanGestureRecognizer!
    private var onDragHorizontalConstraint: NSLayoutConstraint!
    private var onDragVerticalConstraint: NSLayoutConstraint!
    
    private lazy var cellMaxSize = self.tabsCollectionView.frame.width
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabsCollectionView.collectionViewLayout = createLayout()
        tabsCollectionView.register(ItemCollectionViewCell.nib, forCellWithReuseIdentifier: cellID)
        let dropInteraction = UIDropInteraction(delegate: self)
        self.view.addInteraction(dropInteraction)
        reloadBasketButton()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadBasketButton), name: BasketItemsCountCache.countChangedNotification, object: nil)
        
        let unneededConstraints = ["top", traitCollection.layoutDirection == .leftToRight ? "right" : "left"]
        self.shoppingBasketConstraints.filter{unneededConstraints.contains($0.identifier ?? "")}
            .forEach { constraint in
            constraint.isActive = false
        }
        
        basketDragRecognizer = UIPanGestureRecognizer(target: self, action: #selector(onPan))
        basketDragRecognizer.delegate = self
        shoppingBasketButton.addGestureRecognizer(basketDragRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData(animated: false)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        cellMaxSize = size.width
        for cell in self.tabsCollectionView.visibleCells {
            if let widthConstraint = (cell as? ItemCollectionViewCell)?.maxWidthConstraint {
                widthConstraint.constant = cellMaxSize
            }
        }
        coordinator.animate { (context) in
            
        } completion: {[weak self] (context) in
            self?.cellMaxSize = self?.tabsCollectionView.frame.width ?? self?.cellMaxSize ?? 0
            //TODO: selectedIndex path convenience method
            self?.tabsCollectionView.scrollToItem(at: IndexPath(item: self?.selection?.index ?? 0, section: 0), at: .centeredHorizontally, animated: false)
        }
    }
    
    deinit {
            persistTimer?.invalidate()
    }
    
    //MARK: - basket pan
    @objc
    func onPan(recognizer: UIPanGestureRecognizer) {
        if recognizer.view != nil {
            if recognizer.state == .began {
                for constraint in shoppingBasketConstraints {
                    constraint.isActive = false
                }
                
                onDragVerticalConstraint = NSLayoutConstraint(item: shoppingBasketButton!, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: shoppingBasketButton.frame.origin.y)
                onDragHorizontalConstraint = NSLayoutConstraint(item: shoppingBasketButton!, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: shoppingBasketButton.frame.origin.x)
                self.view.addConstraints([onDragHorizontalConstraint, onDragVerticalConstraint])
            }
            else if recognizer.state == .changed {
                let transition  = recognizer.translation(in: self.view)
                onDragHorizontalConstraint.constant += transition.x
                onDragVerticalConstraint.constant += transition.y
                recognizer.setTranslation(CGPoint.zero, in: self.view)
            }
            else if recognizer.state == .ended || recognizer.state == .cancelled {
                let constraintsToAdd: [String]
                switch shoppingBasketButton.center {
                case let p where p.x <= self.view.frame.width/2 && p.y <= self.view.frame.height/2 :
                    constraintsToAdd = ["left", "top"]
                case let p where p.x > self.view.frame.width/2 && p.y <= self.view.frame.height/2 :
                    constraintsToAdd = ["right", "top"]
                case let p where p.x > self.view.frame.width/2 && p.y > self.view.frame.height/2 :
                    constraintsToAdd = ["right", "bottom"]
                case let p where p.x <= self.view.frame.width/2 && p.y > self.view.frame.height/2 :
                    constraintsToAdd = ["left", "bottom"]
                default:
                    constraintsToAdd = ["left", "bottom"]
                }
                
                onDragHorizontalConstraint.isActive = false
                onDragVerticalConstraint.isActive = false
                onDragHorizontalConstraint = nil
                onDragVerticalConstraint = nil
                
                self.shoppingBasketConstraints.filter{constraintsToAdd.contains($0.identifier ?? "")}.forEach { constraint in
                    constraint.isActive = true
                }

                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == basketDragRecognizer { shoppingBasketButton.frame.contains(basketDragRecognizer.location(in: self.view))
        }
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return gestureRecognizer == basketDragRecognizer
    }
    
    //MARK: helper methods
    private func loadData(animated: Bool) {
        //TODO: super simplistic make a network layer and handle errors properly
        let serverEndpoint =
            "https://www.dropbox.com/s/k83imtnhuvjq1yl/ShoppingList.json?dl=1"
        var request = URLRequest(url: URL(string: serverEndpoint)!)
        request.networkServiceType = .responsiveData
        
        URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            if let data = data, self != nil {
                struct Response: Decodable {
                    var categories: [ShoppingCategory]?
                }
                do{
                 let categories = try JSONDecoder().decode(Response.self, from: data)
                    DispatchQueue.main.async { [weak self] in
                        self?.categories = categories.categories
                        self?.reloadUI(animated: animated)
                    }
                }
                catch {
                    print(error)
                }
            }
            //TODO: if there is error on initial loading screen is blank. Put an error message?
        }).resume()
    }
    
    private func reloadUI(animated: Bool){
        
        reloadPages(animated: animated, direction: .forward)
        
        var snapshot = NSDiffableDataSourceSnapshot<Int, ShoppingCategory>()
        snapshot.appendSections([0])
        if let categories = categories {
            snapshot.appendItems(categories)
        }
        dataSource.apply(snapshot, animatingDifferences: false)
        
        //TODO: select cell to a conv. method
        tabsCollectionView.selectItem(at: IndexPath(item: self.selection?.index ?? 0, section: 0), animated: animated, scrollPosition: .centeredHorizontally)
    }
    
    private func reloadPages(animated:Bool, direction: UIPageViewController.NavigationDirection) {
        let viewControllers: [UIViewController]?
        if let viewController = listViewController(for: selection?.index ?? 0) {
            viewControllers = [viewController]
        }
        else {
            viewControllers = nil
        }
        
        self.pageVC.setViewControllers(viewControllers,
                                       direction: direction,
                                       animated: animated,
                                       completion: nil)
    }
    
    private func listViewController(for index: Int) -> UIViewController? {
        if index < categories?.count ?? 0 && index >= 0{
            let page = self.storyboard?.instantiateViewController(identifier: "CategoryItemsList")
            guard let shoppingList = (page as? UINavigationController)?.viewControllers[0] as? ItemsListViewController else {
                assert(false, "storyboard hierarchy unexpected for shopping list")
                return nil
            }
            shoppingList.category = categories?[index]
            
            if let vc = page {
                pages.setObject(index as NSNumber, forKey: vc)
            }
            
            return page
        }
        return nil
    }
    
    @objc
    func reloadBasketButton(){
        self.shoppingBasketButton.setTitle(BasketItemsCountCache.instance.count == 0 ? "" : "\(BasketItemsCountCache.instance.count)", for: .normal)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pageController" {
            pageVC = segue.destination as? UIPageViewController
            pageVC.dataSource = self
            pageVC.delegate = self
        }
        else if segue.identifier == "presentBasket" {
            segue.destination.presentationController?.delegate = self
        }
    }

    //MARK: - UIPageViewControllerDataSource, UIPageViewControllerDelegate
    
    var pages = NSMapTable<UIViewController, NSNumber>.weakToStrongObjects()
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        //Note: this way scroll position is not preserved when changing between screens, but we can load more pages/tabs without memory pressure. I assume best balance between performance/UX would be to cache ~5-10 view controllers with which the user has actually interacted
        let index = pages.object(forKey: viewController)?.intValue ?? selection?.index ?? 0
        let newIndex = index - 1
        let vc = listViewController(for: newIndex)
        
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //TODO: refactor code repetition
        let index = pages.object(forKey: viewController)?.intValue ?? selection?.index ?? -1
        let newIndex = index + 1
        let vc = listViewController(for: newIndex)
        
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let index = pages.object(forKey: pageVC.viewControllers?.first) {
            if let categories  = categories {
                self.selection = (index.intValue, categories[index.intValue])
            }
            
            tabsCollectionView.selectItem(at: IndexPath(item: index.intValue, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    //MARK: -Collection view (tab-like behaviour)
    func createLayout() -> UICollectionViewLayout {
        //TODO: estimated height gives "Unable to simultaneously satisfy constraints", but after layout UI is ok
        let itemSize = NSCollectionLayoutSize(widthDimension: .estimated(1),
                                             heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)

        let groupSize = NSCollectionLayoutSize(widthDimension: .estimated(1),
                                              heightDimension: .fractionalHeight(1))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)

        let section = NSCollectionLayoutSection(group: group)
        let spacing = CGFloat(4)
        section.interGroupSpacing = spacing
        section.contentInsets = NSDirectionalEdgeInsets(top: 3, leading: 8, bottom: 3, trailing: 8)
        section.orthogonalScrollingBehavior = .continuous

        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    private lazy var dataSource: UICollectionViewDiffableDataSource<Int, ShoppingCategory> = {
        return UICollectionViewDiffableDataSource<Int, ShoppingCategory>(collectionView: tabsCollectionView) {
            [weak self] (collectionView: UICollectionView, indexPath: IndexPath, category: ShoppingCategory) -> UICollectionViewCell? in
            
            guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: cellID,
                    for: indexPath) as? ItemCollectionViewCell else {
                fatalError("Cannot create new cell")
            }
            
            cell.label.text = category.name
            cell.label.textColor = category.colour
            
            let maxCellWidth = self?.cellMaxSize ?? collectionView.frame.width
            if cell.maxWidthConstraint == nil {
                cell.maxWidthConstraint = NSLayoutConstraint(item: cell, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: maxCellWidth)
                cell.addConstraint(cell.maxWidthConstraint!)
            }
            else {
                cell.maxWidthConstraint?.constant = maxCellWidth
            }
            
            cell.topAndBottomMargin.constant = 8
            return cell
        }
    }()
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let categories = categories {
            let previousIndex = selection?.index ?? -1
            self.selection = (indexPath.item, categories[indexPath.item])
            reloadPages(animated: true, direction: previousIndex < indexPath.item ? .forward: .reverse)
        }
        
    }
    
    //MARK: UIDropInteractionDelegate
    //TODO: - disable page scrolling while drag and  drop is active
    var persistTimer: Timer?
    
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        if session.localDragSession == nil {
            return false
        }
        for item in session.items {
            if item.localObject is ShoppingItem {
                return true
            }
        }
        return false
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        
        let dropLocation = session.location(in: view)

        //TODO: make buton bigger to indicate it can take the item

            let operation: UIDropOperation

            if shoppingBasketButton.frame.contains(dropLocation) {
                operation = .copy
            } else {
                operation = .cancel
            }
            return UIDropProposal(operation: operation)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        var itemAdded = false
        
        for item in session.items {
            persistTimer?.invalidate()
            persistTimer = nil
            if let shoppingItem = item.localObject as? ShoppingItem {
                CoreDataController.instance.insertEntity(from: shoppingItem)
                itemAdded = true
                persistTimer = Timer.scheduledTimer(withTimeInterval: 6, repeats: false, block: {[weak self]  (timer) in
                    CoreDataController.instance.saveContext()
                    self?.persistTimer?.invalidate()
                    self?.persistTimer = nil
                })
            }
        }
        if !itemAdded {
            ToastViewController.showToast(text: NSLocalizedString("Cannot add item to basket", comment: "") , color: UIColor.red, duration: 5, inWindow: self.view.window)
        }
    }
    
}


extension CategoriesContainerViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
