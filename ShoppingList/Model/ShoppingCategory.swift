//
//  ShoppingCategory.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import UIKit

struct ShoppingCategory: Decodable, Hashable {
    var colour: UIColor?
    //here I'm assuming that category name is unique, but in real lif ethere will probably be id
    var name: String
    var items: [ShoppingItem]?
    
    private enum CodingKeys: String, CodingKey {
            case name, items, colour
        }
    
    init(from decoder: Decoder) throws {
        //TODO test
        let values = try decoder.container(keyedBy: CodingKeys.self)
        colour = try UIColor.color(hex: values.decode(String.self, forKey: .colour))
        name = try values.decode(String.self, forKey: .name)
        items = try values.decode([ShoppingItem].self, forKey: .items)
    }
}
