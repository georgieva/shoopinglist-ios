//
//  ShoppingItem.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import Foundation
import UIKit
import CoreData

struct ShoppingItem: Codable, Hashable, CoreDataPersistable {
    var name: String
    var id = UUID()
    var colour: UIColor?
    
    private enum CodingKeys: String, CodingKey {
            case name
    }
    
    //TODO: can this be done for all model classes with reflection; can it be done with coding keys? better move it to an MO class extension - it is its concern
    func fillInFields(entity: NSManagedObject) {
        if let newItem = entity as? ShoppingItemMO {
            newItem.name = self.name
            //TODO: make categoryColor proper transformable?
            newItem.categoryColor = UIColor.hex(color: self.colour ?? UIColor.white)
        }
    }
}
