//
//  BasketViewController.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import UIKit
import CoreData

class BasketViewController: UIViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var managedObjectContext: NSManagedObjectContext {
        return CoreDataController.instance.persistentContainer.viewContext
    }
    
    private let fetchResultsController: NSFetchedResultsController<ShoppingItemMO> =  {
        let request: NSFetchRequest<ShoppingItemMO> = ShoppingItemMO.fetchRequest()
        let sortDesscriptior = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sortDesscriptior]
        return NSFetchedResultsController(fetchRequest:request , managedObjectContext: CoreDataController.instance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.collectionViewLayout = ItemListUtil.twoColumnLayout()
        collectionView.dataSource = diffableDataSource
        ItemCollectionViewCell.register(collectionView: collectionView)
        self.fetchResultsController.delegate = self
        try? fetchResultsController.performFetch()
        
        self.preferredContentSize = CGSize(width: 700,height: 1000)
    }
    
    lazy var diffableDataSource: UICollectionViewDiffableDataSource<Int, NSManagedObjectID>  = {
        return UICollectionViewDiffableDataSource<Int, NSManagedObjectID>(collectionView: collectionView) {[weak self]  (collectionView, indexPath, objectID) -> UICollectionViewCell? in
            guard let object = try? self?.managedObjectContext.existingObject(with: objectID) as? ShoppingItemMO else {
                fatalError("Managed object should be available")
            }
            
            guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ItemCollectionViewCell.cellID,
                    for: indexPath) as? ItemCollectionViewCell else {
                fatalError("Cannot create new cell")
            }
            
            cell.label.text = object.name
            if let color = object.categoryColor {
                cell.backgroundColor = UIColor.color(hex:color)
            }
            
            return cell
        }
    }()
    
   //MARK:- NSFetchedResultsControllerDelegate
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        guard let dataSource = collectionView?.dataSource as? UICollectionViewDiffableDataSource<Int, NSManagedObjectID> else {
            assertionFailure("The data source has not implemented snapshot support while it should")
            return
        }
        var snapshot = snapshot as NSDiffableDataSourceSnapshot<Int, NSManagedObjectID>
        let currentSnapshot = dataSource.snapshot() as NSDiffableDataSourceSnapshot<Int, NSManagedObjectID>

        let reloadIdentifiers: [NSManagedObjectID] = snapshot.itemIdentifiers.compactMap { itemIdentifier in
            guard let currentIndex = currentSnapshot.indexOfItem(itemIdentifier), let index = snapshot.indexOfItem(itemIdentifier), index == currentIndex else {
                return nil
            }
            guard let existingObject = try? controller.managedObjectContext.existingObject(with: itemIdentifier), existingObject.isUpdated else { return nil }
            return itemIdentifier
        }
        snapshot.reloadItems(reloadIdentifiers)

        let shouldAnimate = collectionView?.numberOfSections != 0
        dataSource.apply(snapshot as NSDiffableDataSourceSnapshot<Int, NSManagedObjectID>, animatingDifferences: shouldAnimate)
        
        collectionView.isHidden = snapshot.numberOfItems == 0
        self.checkoutButton.isEnabled = snapshot.numberOfItems != 0
    }
    
    //MARK: - checkout
    @IBAction func checkout(_ sender: Any) {
        let success = Int.random(in: 0...1)
        let window = self.view.window
        self.presentingViewController?.dismiss(animated: true, completion: {
            if (success != 0) {
                ToastViewController.showToast(text: NSLocalizedString("order completed", comment: ""), color: UIColor.systemGreen, duration: 2, inWindow: window)
                //TODO we delete all items here? nothing in assignment
            }
            else {
                ToastViewController.showToast(text: NSLocalizedString("something went wrong", comment: ""), color: UIColor.systemRed, duration: 5, inWindow: window)
            }
        })

    }
}
