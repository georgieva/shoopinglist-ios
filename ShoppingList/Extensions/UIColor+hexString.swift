//
//  UIColor+codable.swift
//  ShoppingList
//
//  Created by Alexandra Georgieva on 24.10.21.
//

import UIKit

extension UIColor {
    static func color(hex: String)-> UIColor? {
        let red, green, blue: CGFloat
                if hex.hasPrefix("#") {
                    let start = hex.index(hex.startIndex, offsetBy: 1)
                    let hexColor = String(hex[start...])

                    if hexColor.count == 6 {
                        let scanner = Scanner(string: hexColor)
                        var hexNumber: UInt64 = 0

                        if scanner.scanHexInt64(&hexNumber) {
                            red = CGFloat((hexNumber & 0xFF0000) >> 16) / 255
                            green = CGFloat((hexNumber & 0x00FF00) >> 8) / 255
                            blue = CGFloat(hexNumber & 0x0000FF) / 255
                            return UIColor(red: red, green: green, blue: blue, alpha: 1)
                        }
                    }
                }

                return nil
    }
    
    static func hex(color: UIColor)-> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = ((Int)(r*255) & 0xFF)<<16 | ((Int)(g*255) & 0xFF)<<8 | ((Int)(b*255) & 0xFF)
        
        return String(format:"#%06x", rgb)
    }
}
